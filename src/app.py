import json
from flask import Flask
from src.presentation.http.v1.product_controller import product_router
from src.config import configure_inject

configure_inject()

app = Flask(__name__)
app.config["DEBUG"] = True


@app.errorhandler(Exception)
def internal_server_error(e):
    """Error handler"""
    return json.dumps({"error": str(e)}), 500


app.register_blueprint(product_router())  # pylint: disable=E1120
