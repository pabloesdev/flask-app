import inject
from src.domain.ports.input.product_service import ProductService
from src.domain.ports.output.product_repository import ProductRepository
from src.domain.product_service_impl import ProductServiceImpl
from src.infraestructure.adapters.output.product_repository import (
    ProductPostgresRepository,
)


def configure_inject() -> None:
    """Method to configure dependency injections"""

    def config_repositories(binder: inject.Binder) -> None:
        binder.bind_to_provider(ProductRepository, ProductPostgresRepository)

    def config_services(binder: inject.Binder) -> None:
        binder.bind_to_provider(ProductService, ProductServiceImpl)

    def config(binder: inject.Binder) -> None:
        config_repositories(binder)
        config_services(binder)

    inject.configure(config)
