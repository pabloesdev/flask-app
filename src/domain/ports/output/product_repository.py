from typing import List
from abc import ABC, abstractmethod
from src.domain.product import (
    Product,
    ProductCreate,
    ProductCreateOut,
    ProductUpdate,
    ProductUpdateOut,
)


class ProductRepository(ABC):
    @abstractmethod
    def list(self) -> List[Product]:
        ...

    @abstractmethod
    def find(self, product_id: int) -> Product:
        ...

    @abstractmethod
    def find_by_name(self, name: str) -> Product:
        ...

    @abstractmethod
    def find_by_category(self, category_id: int) -> List[Product]:
        ...

    @abstractmethod
    def create(self, product: ProductCreate) -> ProductCreateOut:
        ...

    @abstractmethod
    def update(self, product_id: int, product: ProductUpdate) -> ProductUpdateOut:
        ...

    @abstractmethod
    def delete(self, product_id: int) -> None:
        ...

    @abstractmethod
    def add_transaction(self, transaction_type: str) -> int:
        ...
