from dataclasses import dataclass
from src.domain.utils import validators


@dataclass
class Product:
    """Product Model"""

    id: int
    name: str
    description: str
    price: float
    stock: int
    category_id: int


@dataclass
class ProductCreate:
    name: str = validators.TextValue(min_length=5, max_length=20)  # type: ignore
    description: str = validators.TextValue(min_length=7, max_length=50)  # type: ignore
    price: float = validators.NumericValue(min_value=0, max_value=1000)  # type: ignore
    stock: int = validators.NumericValue(min_value=1, max_value=1000)  # type: ignore
    category_id: int = validators.NumericValue(min_value=1, max_value=10000)  # type: ignore


@dataclass
class ProductCreateOut(Product):
    ...


@dataclass
class ProductUpdate(ProductCreate):
    ...


@dataclass
class ProductUpdateOut(Product):
    ...
