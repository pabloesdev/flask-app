from typing import List
import inject
from src.domain.ports.input.product_service import ProductService
from src.domain.ports.output.product_repository import ProductRepository
from src.domain.product import (
    Product,
    ProductCreate,
    ProductCreateOut,
    ProductUpdate,
    ProductUpdateOut,
)


class ProductServiceImpl(ProductService):
    @inject.autoparams()
    def __init__(self, product_repository: ProductRepository) -> None:
        self._product_repository = product_repository

    def list(self) -> List[Product]:
        return self._product_repository.list()

    def find(self, product_id: int) -> Product:
        return self._product_repository.find(product_id)

    def find_by_name(self, name: str) -> Product:
        return self._product_repository.find_by_name(name)

    def find_by_category(self, category_id: int) -> List[Product]:
        return self._product_repository.find_by_category(category_id)

    def create(self, product: ProductCreate) -> ProductCreateOut:
        return self._product_repository.create(product)

    def update(self, product_id: int, product: ProductUpdate) -> ProductUpdateOut:
        return self._product_repository.update(product_id, product)

    def delete(self, product_id: int) -> None:
        return self._product_repository.delete(product_id)

    def add_product_transaction(self, transaction_type: str) -> int:
        return self._product_repository.add_transaction(transaction_type)
