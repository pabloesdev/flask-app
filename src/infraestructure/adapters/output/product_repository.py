from typing import List, Optional, Any
import logging
from src import get_env_var
from src.domain.product import (
    Product,
    ProductCreate,
    ProductCreateOut,
    ProductUpdate,
    ProductUpdateOut,
)
from dataclasses import asdict
from sqlalchemy import create_engine, insert, update, delete, func
from sqlalchemy.orm import Session
from src.domain.ports.output.product_repository import ProductRepository
from src.domain.product import (
    Product,
    ProductCreate,
    ProductCreateOut,
    ProductUpdate,
    ProductUpdateOut,
)

from src.infraestructure.entities.product_entity import ProductEntity
from src.infraestructure.entities.transaction_emtity import TransactionEntity


class ProductPostgresRepository(ProductRepository):
    def __init__(self) -> None:
        self.engine = create_engine(get_env_var("DATABASE_URL")).connect()

    def list(self) -> List[Product]:
        with Session(self.engine) as session:
            products: List[ProductEntity] = session.query(ProductEntity).all()
            return [
                Product(
                    id=int(product.id),
                    name=str(product.name),
                    description=str(product.description),
                    price=float(product.price),
                    stock=int(product.stock),
                    category_id=int(product.category_id),
                )
                for product in products
            ]

    def find(self, product_id: int) -> Product:
        with Session(self.engine) as session:
            product: ProductEntity | None = (
                session.query(ProductEntity).filter_by(id=product_id).first()
            )
            if product is None:
                raise Exception("Product not found")
            return Product(
                id=int(product.id),
                name=str(product.name),
                description=str(product.description),
                price=float(product.price),
                stock=int(product.stock),
                category_id=int(product.category_id),
            )

    def find_by_name(self, name: str) -> Product:
        with Session(self.engine) as session:
            product: ProductEntity | None = (
                session.query(ProductEntity)
                .filter(func.lower(ProductEntity.name) == name.lower())
                .first()
            )
            if product is None:
                raise Exception("Product not found")
            return Product(
                id=int(product.id),
                name=str(product.name),
                description=str(product.description),
                price=float(product.price),
                stock=int(product.stock),
                category_id=int(product.category_id),
            )

    def find_by_category(self, category_id: int) -> List[Product]:
        with Session(self.engine) as session:
            products: List[ProductEntity] = (
                session.query(ProductEntity).filter_by(category_id=category_id).all()
            )
            return [
                Product(
                    id=int(product.id),
                    name=str(product.name),
                    description=str(product.description),
                    price=float(product.price),
                    stock=int(product.stock),
                    category_id=int(product.category_id),
                )
                for product in products
            ]

    def create(self, product: ProductCreate) -> ProductCreateOut:
        with Session(self.engine) as session:
            stmt = (
                insert(ProductEntity)
                .values(
                    name=product.name,
                    description=product.description,
                    price=product.price,
                    stock=product.stock,
                    category_id=int(product.category_id),
                )
                .returning(ProductEntity.id)
            )
            result: Any = session.execute(statement=stmt)
            session.commit()
            return ProductCreateOut(
                id=result.fetchone()[0],
                name=product.name,
                description=product.description,
                price=product.price,
                stock=product.stock,
                category_id=int(product.category_id),
            )

    def update(self, product_id: int, product: ProductUpdate) -> ProductUpdateOut:
        product_updated = ProductUpdate(**asdict(product))
        with Session(bind=self.engine) as session:
            stmt = (
                update(ProductEntity)
                .where(ProductEntity.id == product_id)
                .values(
                    name=product.name,
                    description=product.description,
                    price=product.price,
                    stock=product.stock,
                    category_id=int(product.category_id),
                )
                .returning(ProductEntity.id)
            )
            result: Any | None = session.execute(statement=stmt)
            if result is None:
                raise Exception("Product not found")

            session.commit()

            return ProductUpdateOut(
                id=result.fetchone()[0],
                name=product.name,
                description=product.description,
                price=product.price,
                stock=product.stock,
                category_id=int(product.category_id),
            )

    def delete(self, product_id: int) -> None:
        with Session(bind=self.engine) as session:
            stmt = delete(ProductEntity).where(ProductEntity.id == product_id)
            session.execute(statement=stmt)
            session.commit()

    def add_transaction(self, transaction_type: str) -> int:
        with Session(self.engine) as session:
            stmt = (
                insert(TransactionEntity)
                .values(transaction_type=transaction_type, user_id=1)
                .returning(TransactionEntity.id)
            )
            result: Any = session.execute(statement=stmt)
            session.commit()
            return result.fetchone()[0]
