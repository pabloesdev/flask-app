from typing import Any
from sqlalchemy.ext.declarative import declarative_base

# from sqlalchemy.orm import DeclarativeMeta

Base: Any = declarative_base()

from src.infraestructure.entities.product_entity import ProductEntity  # noqa: F401 E402
from src.infraestructure.entities.transaction_emtity import (
    TransactionEntity,
)  # noqa: F401 E402
