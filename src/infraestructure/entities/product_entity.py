from sqlalchemy import Column, Integer, VARCHAR, Float
from src.infraestructure.entities import Base


class ProductEntity(Base):
    __tablename__ = "products"
    id = Column(Integer(), primary_key=True)
    name = Column(VARCHAR(length=25), nullable=False)
    description = Column(VARCHAR(length=100), nullable=False)
    price = Column(Float(), nullable=False)
    stock = Column(Integer(), nullable=False)
    category_id = Column(Integer(), nullable=False)
