from sqlalchemy import Column, Integer, VARCHAR, Float, DateTime, func
from src.infraestructure.entities import Base


class TransactionEntity(Base):
    __tablename__ = "transactions"
    id = Column(Integer(), primary_key=True)
    transaction_type = Column(VARCHAR(length=100), nullable=False)
    user_id = Column(Integer(), nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
