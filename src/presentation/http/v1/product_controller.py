import json
from dataclasses import asdict
import inject
from flask import Blueprint, Response, request
from src.domain.ports.input.product_service import ProductService
from src.domain.product import ProductCreate, ProductUpdate
from flask_httpauth import HTTPTokenAuth  # type: ignore


auth: HTTPTokenAuth = HTTPTokenAuth(scheme="Bearer")
test_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"


@auth.verify_token
def verify_token(token):
    return token == test_token


@inject.autoparams()
def product_router(product_service: ProductService) -> Blueprint:
    """Return a flask blueprint with products http method"""

    product_blueprint = Blueprint("product", __name__, url_prefix="/api/v1/products")

    @product_blueprint.get("/")
    @auth.login_required
    def get_products() -> Response:
        products = product_service.list()
        response = [asdict(product) for product in products]
        transaction_id = product_service.add_product_transaction("LIST_PRODUCTS")
        return Response(
            response=json.dumps({"transaction_id": transaction_id, "data": response}),
            status=200,
        )

    @product_blueprint.get("/<product_id>")
    @auth.login_required
    def get_product(product_id: int) -> Response:
        product = product_service.find(product_id)
        response = asdict(product)
        transaction_id = product_service.add_product_transaction("FIND_PRODUCT")
        return Response(
            response=json.dumps({"transaction_id": transaction_id, "data": response}),
            status=200,
        )

    @product_blueprint.get("/search/")
    @auth.login_required
    def get_product_by_name() -> Response:
        name = request.args.get("name", default="", type=str)
        product = product_service.find_by_name(name)
        response = asdict(product)
        transaction_id = product_service.add_product_transaction("FIND_PRODUCT")
        return Response(
            response=json.dumps({"transaction_id": transaction_id, "data": response}),
            status=200,
        )

    @product_blueprint.get("/category/<category_id>")
    @auth.login_required
    def get_product_by_category_id(category_id: int) -> Response:
        products = product_service.find_by_category(category_id)
        response = [asdict(product) for product in products]
        transaction_id = product_service.add_product_transaction("FIND_PRODUCT")
        return Response(
            response=json.dumps({"transaction_id": transaction_id, "data": response}),
            status=200,
        )

    @product_blueprint.post("/")
    @auth.login_required
    def create_product() -> Response:
        data = request.json if request.json else {}
        product = ProductCreate(**data)
        product_created = product_service.create(product)
        response = asdict(product_created)
        transaction_id = product_service.add_product_transaction("ADD_PRODUCT")
        return Response(
            response=json.dumps({"transaction_id": transaction_id, "data": response}),
            status=201,
        )

    @product_blueprint.put("/<product_id>")
    @auth.login_required
    def update_product(product_id: int) -> Response:
        data = request.json if request.json else {}
        product = ProductUpdate(**data)
        product_updated = product_service.update(product_id, product)
        response = asdict(product_updated)
        transaction_id = product_service.add_product_transaction("UPDATE_PRODUCT")
        return Response(
            response=json.dumps({"transaction_id": transaction_id, "data": response}),
            status=200,
        )

    @product_blueprint.delete("/<product_id>")
    @auth.login_required
    def delete_product(product_id: int) -> Response:
        product_service.delete(product_id)
        transaction_id = product_service.add_product_transaction("DELETE_PRODUCT")
        return Response(
            response=json.dumps({"transaction_id": transaction_id, "data": "Deleted."}),
            status=200,
        )

    return product_blueprint
